﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Albaka_Productions
{
    public partial class SignIn : System.Web.UI.Page
    {
        public string msg;
        public string sqlLogin;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form["submit"] != null)
            {
                string uName = Request.Form["uName"];
                string pass = Request.Form["pw"];

                string fileName = "App_Data.mdf";
                string tableName = "usersTbl";

                sqlLogin = "SELECT * FROM " + tableName + " WHERE uName = '" + uName + "' AND password = '" + pass + "'";
                if (Helper.IsExist(fileName, sqlLogin))
                {
                    DataTable table = Helper.ExecuteDataTable(fileName, sqlLogin);

                    int length = table.Rows.Count;
                    if (length == 0)
                    {
                        msg = "משתמש לא נמצא";
                    }
                    else
                    {
                        Session["email"] = table.Rows[0]["email"];
                        Session["fName"] = table.Rows[0]["fName"];
                        Response.Redirect("HomePage.aspx");
                    }
                }
                else
                {
                    msg = "המשתמש לא נמצא";
                }
            }
        }
    }
}