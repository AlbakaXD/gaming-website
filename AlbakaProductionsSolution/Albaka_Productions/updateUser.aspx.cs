﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Albaka_Productions
{
    public partial class updateUser : System.Web.UI.Page
    {
        public string msg = "";
        public string sqlUpdate = "";
        public string sqlSelect = "";

        public string uName, fName, sName, email, password;
        public string Playstation, Nintendo, Xbox, PC, VR;
        public string ganre;
        protected void Page_Load(object sender, EventArgs e)
        {
            string fileName = "App_Data.mdf";
            email = Session["email"].ToString();

            if (uName == "אורח")
            {
                msg = "אינך רשום במערכת";
                Response.Redirect("HomePage.aspx");
            }

            sqlSelect = "SELECT * FROM usersTbl WHERE email = '" + email + "'";
            DataTable table = Helper.ExecuteDataTable(fileName, sqlSelect);

            int length = table.Rows.Count;
            if (length == 0)
            {
                msg = "אינך רשום במערכת";
            }
            else
            {
                uName = table.Rows[0]["uName"].ToString().Trim();
                fName = table.Rows[0]["fName"].ToString().Trim();
                sName = table.Rows[0]["sName"].ToString().Trim();
                password = table.Rows[0]["password"].ToString().Trim();
                Playstation = table.Rows[0]["Playstation"].ToString().Trim();
                Nintendo = table.Rows[0]["Nintendo"].ToString().Trim();
                Xbox = table.Rows[0]["Xbox"].ToString().Trim();
                PC = table.Rows[0]["PC"].ToString().Trim();
                VR = table.Rows[0]["VR"].ToString().Trim();
                ganre = table.Rows[0]["ganre"].ToString().Trim();

                if (this.IsPostBack)
                {
                    fName = Request.Form["fName"];
                    uName = Request.Form["uName"];
                    sName = Request.Form["sName"];
                    password = Request.Form["password"];
                    ganre = Request.Form["ganre"];
                    string console = Request.Form["console"].ToString();
                    Playstation = "F";
                    Nintendo = "F";
                    Xbox = "F";
                    PC = "F";
                    VR = "F";

                    if (console.Contains('1')) Playstation = "T";
                    if (console.Contains('2')) Nintendo = "T";
                    if (console.Contains('3')) Xbox = "T";
                    if (console.Contains('4')) PC = "T";
                    if (console.Contains('5')) VR = "T";

                    sqlUpdate = "UPDATE usersTbl ";
                    sqlUpdate = sqlUpdate + "SET fName = '" + fName + "', ";
                    sqlUpdate = sqlUpdate + "uName = '" + uName + "', ";
                    sqlUpdate = sqlUpdate + "sName = '" + sName + "', ";
                    sqlUpdate = sqlUpdate + "password = '" + password + "', ";
                    sqlUpdate = sqlUpdate + "ganre = '" + ganre + "', ";
                    sqlUpdate = sqlUpdate + "Playstation = '" + Playstation + "', ";
                    sqlUpdate = sqlUpdate + "Nintendo = '" + Nintendo + "', ";
                    sqlUpdate = sqlUpdate + "Xbox = '" + Xbox + "', ";
                    sqlUpdate = sqlUpdate + "PC = '" + PC + "', ";
                    sqlUpdate = sqlUpdate + "VR = '" + VR + "' ";
                    sqlUpdate = sqlUpdate + "WHERE email = '" + email + "'";

                    Helper.DoQuery(fileName, sqlUpdate);

                    msg = "Succses";
                }
            }
        }
    }
}