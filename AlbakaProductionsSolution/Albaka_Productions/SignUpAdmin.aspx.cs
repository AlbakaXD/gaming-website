﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Albaka_Productions
{
    public partial class SignUpAdmin : System.Web.UI.Page
    {
        public string msg;
        public string sqlLogin;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form["submit"] != null)
            {
                string uName = Request.Form["uName"];
                string pw = Request.Form["pw"];

                string fileName = "App_Data.mdf";
                string tableName = "managerTbl";

                 sqlLogin = "SELECT * FROM " + tableName + " WHERE uName = '" + uName + "' AND password = '" + pw + "'";
                DataTable table = Helper.ExecuteDataTable(fileName, sqlLogin);

                int length = table.Rows.Count;
                if (length == 0)
                {
                    msg = "<div style='text-align:center;'>";
                    msg = msg + "<h3>אינך מנהל/ת אין לך הרשאה לצפות בדף זה</h3>";
                    msg = msg + "<a href='HomePage.aspx' style='color:#e86400'> המשך </a>";
                    msg = msg + "</div>";
                }
                else
                {
                    Session["fName"] = "מנהל";
                    Session["admin"] = "yes";
                    Response.Redirect("HomePage.aspx");
                }
            }
        }
    }
}