﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SimpleQuery.aspx.cs" Inherits="Albaka_Productions.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>הצגת נתונים לפי חתך</h1>
    <form method="post" runat="server">
        <select name="field" id="field" onclick="detectField();">
            <option value="uName">שם משתמש</option>
            <option value="fName">שם פרטי</option>
            <option value="sName">שם משפחה</option>
            <option value="email">אימייל</option>
            <option value="console">קונסולה</option>
            <option value="ganre">ז'אנר</option>
        </select>
        <div id ="query"></div>
        <br /><br />
        <input type="submit" name="submit" value="חפש" />
    </form>


    <script>
        function detectField()
        {
            if (document.getElementById("field").value == "console")
            {
                var consolestr = "<select name='value'><option value='0'>בחר קונסולה</option>"
                consolestr = consolestr + "<option value='1'>Playstation</option>";
                consolestr = consolestr + "<option value='2'>Nintendo</option>";
                consolestr = consolestr + "<option value='3'>Xbox</option>";
                consolestr = consolestr + "<option value='4'>PC</option>";
                consolestr = consolestr + "<option value='5'>VR</option>";

                consolestr = consolestr + "</select>";

                document.getElementById("query").innerHTML = consolestr;
            }

            else if (document.getElementById("field").value == "ganre") {
                var ganrestr = "<select name='value'><option value='0'>בחר ז'אנר</option>";
                ganrestr = ganrestr + "<option value='FPS'>FPS</option>";
                ganrestr = ganrestr + "<option value='puzzle'>puzzle</option>";
                ganrestr = ganrestr + "<option value='platform'>platform</option>";
                ganrestr = ganrestr + "<option value='horror'>horror</option>";
                ganrestr = ganrestr + "<option value='RPG'>RPG</option>";
                ganrestr = ganrestr + "<option value='adventure'>adventure</option>";
                ganrestr = ganrestr + "<option value='co-op'>co-op</option>";
                ganrestr = ganrestr + "<option value='multyplayer'>multyplayer</option>";
                ganrestr = ganrestr + "<option value='sports'>sports</option>";
                ganrestr = ganrestr + "<option value='VRgame'>VR</option>";
                ganrestr = ganrestr + "<option value='open world'>open world</option>";

                ganrestr = ganrestr + "</select>";

                document.getElementById("query").innerHTML = ganrestr;
            }
            else
            {
                document.getElementById("query").innerHTML = "<input type='text' name='value'/>";
            }
        }
    </script>
    <table style="border: 1px solid; margin: 0px auto;">
        <%= st %>
    </table>

    <h3><%= msg %></h3>
</asp:Content>

