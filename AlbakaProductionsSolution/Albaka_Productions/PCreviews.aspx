﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="PCreviews.aspx.cs" Inherits="Albaka_Productions.PCreviews1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <h2 style="color:#e86400; direction:rtl; text-align:center">ביקורות PC</h2>

    <img src="Styles/AlbakaProductions%20pictures/3535992-most-influential-half-life2-thumbnologo.jpg" width="360" height="202" style="border:0; float:left; padding:20px"/>
    <h3 style="color:#e86400; text-align:right">Half Life 2</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
        משחק ההמשך של חברת המשחקים ושל חנות המשחקים הדיגיטלית Valve<br />
        למה Half Life 2 נחשב לאחד ממשחקי המחשב הטובים והמהפכניים ביותר<br />
        ואיך הוא מחזיק היום לאחר יותר מעשור וחצי?<br />
    </h4>
    <p style="color:white; direction:rtl; text-align:right">
        Half Life הוא משחק Singel-player Action FPS שפותח על ידי החברה המוכרת Valve.<br />
        המשחק יצא לקהל הרחב ב-16 בנובמבר 2004 ונחשב עד היום לטענתם של גיימרים רבים למשחק היריות הטוב ביותר<br />
        יש שיגידו שחולל שינוי בלתי הפיך בתעשיית משחקי המחשב, החלטתי לחזור למשחק פעם נוספת ולראות אם הוא מחזיק מעמד גם היום...<br />
        לביקורת המלאה:<br />
        <a href="HalfLife2.aspx" style="color:#e86400">Half Life 2</a><br />
        <br />
    </p>
</asp:Content>
