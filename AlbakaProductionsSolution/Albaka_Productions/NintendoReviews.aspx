﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="NintendoReviews.aspx.cs" Inherits="Albaka_Productions.NintendoReviews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="color:#e86400; direction:rtl; text-align:center">ביקורות Nintendo</h2>

    <img src="Styles/AlbakaProductions%20pictures/Switch_SuperMarioMaker_ND0515_screen_03.0.jpg" width="360" height="202" style="border:0; float:left; padding:20px"/>
    <h3 style="color:#e86400; text-align:right">Super Mario Maker 2</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
        בשביל מה לפתח משחק כשאפשר שהשחקנים יעשו את זה בשבילנו<br />
        עם מגוון רחב של אפשרויות וקהילה מצויינת<br />
        אין סיכוי שלא תמצאו משהו שתאהבו<br />
    </h4>
    <p style="color:white; direction:rtl; text-align:right">
        Nintendo החליטו לעלות מחדש על גל ההצלחה של המשחק הקודם בסדרה ושיחררו את המשחק Super Mario Maker 2<br />
        במקום להכריח את באוזר לחטוף את הנסיכה פיץ' בפעם המליון נקבל את האפשרות ליצור שלבים משלנו, לשחק בשלבים שבנו שחקנים אחרים<br />
        ואפילו נקבל את מצב ה-Story שכולל מעל 100 שלבים שפיתחו צוות המשחק...<br />
        לביקורת המלאה:<br />
        <a href="SuperMarioMaker2.aspx" style="color:#e86400">Super Mario Maker 2</a><br />
    </p>
</asp:Content>
