﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="updateUser.aspx.cs" Inherits="Albaka_Productions.updateUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        h1{text-align:center; color:#e86400}
        h2{text-align:center; color:#e86400}
        h3{text-align:center; color:white}
        td{width:300px}
        th{color:white; font-family: 'Guttman Yad-Brush';}
        table{margin: 0px auto;}
        .formTbl{margin: 0px auto; border: 1px solid;}
        .formTd{border: 1px solid;}
    </style>
    <h1>טופס לעדכון פרטים</h1>

                    <script>
                        function isHebres(mail) {
                            var len = mail.length;
                            var i = 0, ch;
                            while (i < len) {
                                ch = mail.charAt(i);
                                if (ch >= 'א' && ch <= 'ת')
                                    return false;
                                i++
                            }
                            return true;
                        }

                        function isValidString(mail) {
                            var badChr = "$%^&*()-![]{}<>?";
                            var len = mail.length;
                            var i = 0, pos, ch;
                            while (i < len) {
                                ch = mail.charAt(i);
                                pos = badChr.indexOf(ch);
                                if (pos != -1)
                                    return false;
                                i++;
                            }
                            return true;
                        }

                        function chkForm() {
                            var flag = true;
                            var uName = document.getElementById("uName").value;
                            if (uName.length < 2) {
                                document.getElementById("muName").value = "שם משתמש קצר מידי";
                                document.getElementById("muName").style.display = "inline";
                                flag = false;
                            }
                            else if (uName.length > 10) {
                                document.getElementById("muName").value = "שם משתמש ארוך מידי";
                                document.getElementById("muName").style.display = "inline";
                                flag = false;
                            }
                            else
                                document.getElementById("muName").style.display = "none";


                            var fName = document.getElementById("fName").value;
                            if (fName.length < 2) {
                                document.getElementById("mfName").value = "שם פרטי קצר מידי או לא קיים";
                                document.getElementById("mfName").style.display = "inline";
                                flag = false;
                            }
                            else if (fName.length > 10) {
                                document.getElementById("mfName").value = "שם פרטי ארוך מידי";
                                document.getElementById("mfName").style.display = "inline";
                                flag = false;
                            }
                            else
                                document.getElementById("mfName").style.display = "none";


                            var sName = document.getElementById("sName").value;
                            if (sName.length < 2) {
                                document.getElementById("msName").value = "שם משפחה קצר מידי או לא קיים";
                                document.getElementById("msName").style.display = "inline";
                                flag = false;
                            }
                            else if (sName.length > 10) {
                                document.getElementById("msName").value = "שם משפחה קצר מידי או לא קיים";
                                document.getElementById("msName").style.display = "inline";
                                flag = false;
                            }
                            else
                                document.getElementById("msName").style.display = "none";


                            var email = document.getElementById("email").value;
                            var size = email.length;
                            var atSign = email.indexOf('@');
                            var dotSign = email.indexOf('.', atSign);

                            var msg = " ";
                            if (size < 6)
                                msg = "כתובת האימייל קצרה מידי או לא קיימת";
                            else if (atSign == -1)
                                msg = "הסימן @ לא מופיע בכתובת האימייל";
                            else if (atSign != email.lastIndexOf('@'))
                                msg = "שני סימני @ או יותר לא תקפים בכתובת אימייל";
                            else if (atSign < 2 || email.lastIndexOf('@') == size - 1)
                                msg = "הסימן @ לא יכול להופיע בתחילת האימייל או בסופו";
                            else if (email.indexOf('.') == 0 || email.lastIndexOf('.') == size - 1)
                                msg = "נקודה לא יכולה להיות בתחילת אימייל או בסופו";
                            else if (dotSign - atSign <= -1)
                                msg = "הנקודה חייבת להיות לפחות שני תווים לאחר הסימן @";
                            else if (isHebres(email) == false)
                                msg = "כתובת האימייל לא יכולה להכיל אותיות בעברית"
                            else if (isValidString(email) == false)
                                msg = "כתובת האימייל לא יכולה להכיל תווים שהם לא נקודה או שטרודל"

                            if (msg != " ") {
                                document.getElementById("memail").value = msg;
                                document.getElementById("memail").style.display = "inline";
                                flag = false;
                            }
                            else
                                document.getElementById("memail").style.display = "none";


                            var password = document.getElementById("password").value;
                            var passCheckletter = false;
                            var passChecknumber = false;
                            password_length = password.length;
                            for (var i = 0; i < password_length; i++) {
                                if (password[i] <= 'z' && password[i] >= 'a' || password[i] <= 'Z' && password[i] >= 'A') {
                                    passCheckletter = true;
                                }
                                if (password[i] <= 9 && password[i] >= 0) {
                                    passChecknumber = true;
                                }
                            }
                            if (password_length <= 6 || password_length >= 12) {
                                document.getElementById("mPassword").value = "אורך סיסמא לא תקין";
                                document.getElementById("mPassword").style.display = "inline";
                                flag = false;
                            }
                            else if (passCheckletter == false) {
                                document.getElementById("mPassword").value = "בסיסמא חייבת להיות אות אחת לפחות";
                                document.getElementById("mPassword").style.display = "inline";
                                flag = false;
                            }
                            else if (passChecknumber == false) {
                                document.getElementById("mPassword").value = "בסיסמא חייבת להיות ספרה אחת לפחות";
                                document.getElementById("mPassword").style.display = "inline";
                                flag = false;
                            }
                            else
                                document.getElementById("mPassword").style.display = "none";


                            var passwordConfirm = document.getElementById("passwordConfirm").value;
                            var count = 0;
                            for (var i = 0; i < password_length; i++) {
                                if (password[i] == passwordConfirm[i]) {
                                    count = count + 1;
                                }
                            }
                            if (count != password_length) {
                                document.getElementById("mPasswordConfirm").value = "הסיסמאות לא דומות";
                                document.getElementById("mPasswordConfirm").style.display = "inline";
                                flag = false;
                            }
                            else
                                document.getElementById("mPasswordConfirm").style.display = "none";


                            var type = document.getElementsByName("type");
                            var ansChecked = false;
                            for (var i = 0; i < type.length; i++) {
                                if (type[i].checked == true) {
                                    ansChecked = true;
                                }
                            }
                            if (ansChecked == false) {
                                document.getElementById("mtype").value = "ז'אנר לא נבחר";
                                document.getElementById("mtype").style.display = "inline";
                                flag = false;
                            }
                            else
                                document.getElementById("mtype").style.display = "none";


                            var time = document.getElementById("time");
                            if (time.selectedIndex == 0) {
                                document.getElementById("mtime").value = "תשובה לא נבחרה";
                                document.getElementById("mtime").style.display = "inline";
                                flag = false;
                            }
                            else
                                document.getElementById("mtime").style.display = "none";


                            var console = document.getElementsByName("console");
                            var consoleChecked = false;
                            for (var i = 0; i < console.length; i++) {
                                if (console[i].checked) {
                                    consoleChecked = true;
                                }
                            }
                            if (consoleChecked == false) {
                                document.getElementById("mconsole").value = "לא נבחרה קונסולה ";
                                document.getElementById("mconsole").style.display = "inline";
                                flag = false;
                            }
                            else {
                                document.getElementById("mconsole").style.display = "none";
                            }
                            return flag;
                        }
            </script>
    <form method="post" runat="server" onsubmit="return chkForm();">
        <table>
        <tr>
            <td>אימייל</td>
            <td>
                <input type="text" name="email" disabled="disabled" value="<%= email %>" />
            </td>
        </tr>
        <tr>
            <td>שם משתמש</td>
            <td>
                <input type="text" id="uName" name="uName" value="<%= uName %>" />
            </td>
            <td>
                <input type="text" id="mUname" size="30" style="display:none;background-color:silver;font-weight:bold;" disabled ="disabled" />
            </td>
        </tr>
        <tr>
            <td>שם פרטי</td>
            <td>
                <input type="text" id="fName" name="fName" value="<%= fName %>" />
            </td>
            <td>
                <input type="text" id="mFname" size="30" style="display:none;background-color:silver;font-weight:bold;" disabled ="disabled" />
            </td>
        </tr>
        <tr>
            <td>שם משפחה</td>
            <td>
                <input type="text" id="sName" name="sName" value="<%= sName %>" />
            </td>
            <td>
                <input type="text" id="mSname" size="30" style="display:none;background-color:silver;font-weight:bold;" disabled ="disabled" />
            </td>
        </tr>
        <tr>
            <td>סיסמה</td>
            <td>
                <input type="password" id="password" name="password" value="<%= password %>" maxlength ="12"/>
            </td>
            <td>
                <input type="text" id="mPassword" size="30" style="display:none;background-color:silver;font-weight:bold;" disabled ="disabled" />
            </td>
        </tr>
        <tr>
            <td>ז'אנר</td>
            <td>
                <%if (ganre == "FPS")
                    { %>
                <input type="radio" name="ganre" value="FPS" checked  /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                 <%}
                     else if (ganre == "puzzle")
                     { %>
                <input type="radio" name="ganre" value="FPS"   /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle" checked /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                <%}
                    else if (ganre == "platform")
                    { %>
                <input type="radio" name="ganre" value="FPS"  /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform" checked /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                 <%}
                     else if (ganre == "horror")
                     { %>
                <input type="radio" name="ganre" value="FPS"   /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror" checked /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                 <%}
                     else if (ganre == "RPG")
                     { %>
                <input type="radio" name="ganre" value="FPS"   /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG" checked /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                 <%}
                     else if (ganre == "adventure")
                     { %>
                <input type="radio" name="ganre" value="FPS"   /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure" checked /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                                 <%}
                     else if (ganre == "co-op")
                     { %>
                <input type="radio" name="ganre" value="FPS"   /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op" checked /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                                 <%}
                     else if (ganre == "multyplayer")
                     { %>
                <input type="radio" name="ganre" value="FPS"   /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer" checked /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                                 <%}
                     else if (ganre == "sports")
                     { %>
                <input type="radio" name="ganre" value="FPS"   /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports" checked /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                <%}
                     else if (ganre == "VR")
                     { %>
                <input type="radio" name="ganre" value="FPS"   /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR" checked /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld"  /style="color:white;"> open world
                <%}
                    else
                    {%>
                <input type="radio" name="ganre" value="FPS" /style="color:white;"/>FPS
                <input type="radio" name="ganre" value="puzzle"  /style="color:white;"> puzzle
                <input type="radio" name="ganre" value="platform"  /style="color:white;"> platform
                <input type="radio" name="ganre" value="horror"  /style="color:white;"> horror
                <input type="radio" name="ganre" value="RPG"  /style="color:white;"> RPG
                <input type="radio" name="ganre" value="adventure"  /style="color:white;"> adventure 
                <input type="radio" name="ganre" value="co-op"  /style="color:white;"> co-op 
                <input type="radio" name="ganre" value="multyplayer"  /style="color:white;"> multyplayer
                <input type="radio" name="ganre" value="sports"  /style="color:white;"> sports 
                <input type="radio" name="ganre" value="VR"  /style="color:white;"> VR 
                <input type="radio" name="ganre" value="openWorld" checked /style="color:white;"> open world
                <%} %>
            </td>
        </tr>
        <tr>
            <td>קונסולה</td>
                  <td>
                      <table style="width:550px;">
                          <tr>
                          <td class ="formTbl">
                              <%if (Playstation == "T")
                                  { %>
                              <input type="checkbox" name="console" value="1" checked="checked" />
                              <%}
    else
    { %>
                              <input type="checkbox" name="console" value="1" />
                              <%} %> Playstation
                          </td>
                          <td class="formTbl">
                              <%if (Nintendo == "T")
                                  { %>
                              <input type="checkbox" name="console" value="2" checked="checked" />
                              <%}
    else
    { %>
                              <input type="checkbox" name="console" value="2" />
                              <%} %> Nintendo
                          </td>
                          <td class ="formTbl">
                              <%if (Xbox == "T")
                                  { %>
                              <input type="checkbox" name="console" value="3" checked="checked" />
                              <%}
    else
    { %>
                              <input type="checkbox" name="console" value="3" />
                              <%} %> Xbox
                          </td>
                          <td class ="formTbl">
                              <%if (PC == "T")
                                  { %>
                              <input type="checkbox" name="console" value="4" checked="checked" />
                              <%}
    else
    { %>
                              <input type="checkbox" name="console" value="4" />
                              <%} %> PC
                          </td>
                           <td class ="formTbl">
                              <%if (VR == "T")
                                  { %>
                              <input type="checkbox" name="console" value="5" checked="checked" />
                              <%}
    else
    { %>
                              <input type="checkbox" name="console" value="5" />
                              <%} %> VR
                          </td>
                      </tr>

                      </table>
                </td>
            </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <input type="submit" name="submit" value=" עדכן " />
            </td>
        </tr>
    </table>

    <h3><%= msg %></h3>
</form>

</asp:Content>
