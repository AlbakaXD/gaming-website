﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Albaka_Productions
{
    public partial class deleteUser : System.Web.UI.Page
    {
        public string st;
        public string msg;
        public string sqlDelete;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["admin"].ToString() == "no")
            {
                msg = "<div style='text-align:center;'>";
                msg = msg + "<h3>אינך מנהל/ת אין לך הרשאה לצפות בדף זה</h3>";
                msg = msg + "<a href='HomePage.aspx' style='color:#e86400'> המשך </a>";
                msg = msg + "</div>";
            }
            else
            {
                string fileName = "App_Data.mdf";
                string tableName = "usersTbl";
                string sqlSelect = "SELECT * FROM " + tableName;
                DataTable table = Helper.ExecuteDataTable(fileName, sqlSelect);
                string userToDelete = "";
                int length = table.Rows.Count;
                if (length == 0)
                    msg = "לא נמצאו רשומות לחיפוש";
                else
                {
                    st += "<tr>";
                    st += "<th style = 'text-align: center; border: 1px solid white; width: 100px;'>שם משתמש</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white; width: 80px;'>שם פרטי</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white; width: 60px;'>שם משפחה</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white; width: 140px;'>דואל</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white;'>Playstation</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white;'>Nintendo</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white;'>Xbox</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white;'>PC</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white;'>VR</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white;'>ganre</ th>";
                    st += "<th style = 'text-align: center; border: 1px solid white;'> </ th>";
                    st += "</tr>";

                    for (int i = 0; i < length; i++)
                    {
                        st += "<tr>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white;'>" + table.Rows[i]["uName"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white; width: 80px;'>" + table.Rows[i]["fName"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white; width: 80px;'>" + table.Rows[i]["sName"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white; width: 140px;'>" + table.Rows[i]["email"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white;'>" + table.Rows[i]["Playstation"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white;'>" + table.Rows[i]["Nintendo"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white;'>" + table.Rows[i]["Xbox"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white;'>" + table.Rows[i]["PC"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white;'>" + table.Rows[i]["VR"] + "</td>";
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white;'>" + table.Rows[i]["ganre"] + "</td>";
                        userToDelete = table.Rows[i]["uName"].ToString();
                        st += "<td style = 'text-align: center; border: 1px solid white; color:white;'>";
                        st += "<a href = 'DeleteRecord.aspx?uName=" + userToDelete + "'>[delete]</a>";
                        st += "</tr>";
                    }
                }
            }
        }
    }
}