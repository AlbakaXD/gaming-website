﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="Albaka_Productions.HomePage1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <a href="#lastreviews" style="color:#e86400; text-align:center">הביקורות האחרונות ביותר</a>
    <a href="#news" style="color:#e86400; text-align:center">חדשות</a>
    <br />
    <a id="lastreviews" style="color:#e86400; text-align:right;">
        <h1>הביקורות האחרונות ביותר</h1>
    </a>
    <img src="Styles/AlbakaProductions%20pictures/3535992-most-influential-half-life2-thumbnologo.jpg" width="360" height="202" style="border:0; float:left; padding:20px"/>
    <h3 style="color:#e86400; text-align:right">Half Life 2</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
        משחק ההמשך של חברת המשחקים ושל חנות המשחקים הדיגיטלית Valve<br />
        למה Half Life 2 נחשב לאחד ממשחקי המחשב הטובים והמהפכניים ביותר<br />
        ואיך הוא מחזיק היום לאחר יותר מעשור וחצי?<br />
    </h4>
    <p style="color:white; direction:rtl; text-align:right">
        Half Life הוא משחק Singel-player Action FPS שפותח על ידי החברה המוכרת Valve.<br />
        המשחק יצא לקהל הרחב ב-16 בנובמבר 2004 ונחשב עד היום לטענתם של גיימרים רבים למשחק היריות הטוב ביותר<br />
        יש שיגידו שחולל שינוי בלתי הפיך בתעשיית משחקי המחשב, החלטתי לחזור למשחק פעם נוספת ולראות אם הוא מחזיק מעמד גם היום...<br />
        לביקורת המלאה:<br />
        <a href="HalfLife2.aspx" style="color:#e86400">Half Life 2</a><br />
        <br />
    </p>
    <img src="Styles/AlbakaProductions%20pictures/the-last-guardian-desktop-1920x1080-wallpaper-01-04nov16.jpg" width="360" height="202" style="border:0; float:left; padding:20px" />
    <h3 style="color:#e86400; text-align:right">The Last Guardian</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
        משחק כל כך יפה שלא יכולתי שלא להישבות בקסמיו!!!<br />
        The Last Guardian ל-PlayStation 4 הוא רימייק למשחק שיצא ל-PlayStation 2, מה חדש ומה השתנה?
    </h4>
    <p style="color:white; direction:rtl; text-align:right">
        The Last Guardian הוא משחק Puzzle-Adventure בלעדי ל-PlayStation 4<br />
        המשחק הוא פורט שהגיע מה-PlayStation 2, יצאתי לבדוק מה השתנה ומה נשמר<br />
        המשחק הזה לא דומה לרבים בתעשיית המשחקים<br />
        הוא רגוע ושליו ולעיתים מפחיד ומלחיץ...<br />
        לביקורת המלאה:<br />
        <a href="TheLastGuardian.aspx" style="color:#e86400">The Last Guardian</a><br />
        <br />
    </p>
    <img src="Styles/AlbakaProductions%20pictures/Switch_SuperMarioMaker_ND0515_screen_03.0.jpg" width="360" height="202" style="border:0; float:left; padding:20px"/>
    <h3 style="color:#e86400; text-align:right">Super Mario Maker 2</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
        בשביל מה לפתח משחק כשאפשר שהשחקנים יעשו את זה בשבילנו<br />
        עם מגוון רחב של אפשרויות וקהילה מצויינת<br />
        אין סיכוי שלא תמצאו משהו שתאהבו<br />
    </h4>
    <p style="color:white; direction:rtl; text-align:right">
        Nintendo החליטו לעלות מחדש על גל ההצלחה של המשחק הקודם בסדרה ושיחררו את המשחק Super Mario Maker 2<br />
        במקום להכריח את באוזר לחטוף את הנסיכה פיץ' בפעם המליון נקבל את האפשרות ליצור שלבים משלנו, לשחק בשלבים שבנו שחקנים אחרים<br />
        ואפילו נקבל את מצב ה-Story שכולל מעל 100 שלבים שפיתחו צוות המשחק...<br />
        לביקורת המלאה:<br />
        <a href="SuperMarioMaker2.aspx" style="color:#e86400">Super Mario Maker 2</a><br />
    </p>

    <a id="news" style="color:#e86400; text-align:right;">
        <h1>חדשות</h1>
    </a>

    <h3 style="color:#e86400; text-align:right">Nightmare city</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
        CD PROJEKT RED שיחררו סופסוף את המשחק cyberpunk 2077<br />
       ובלי להפריז יותר מידי במילים אפשר לומר שהוא בין המשחקים הפחות טובים שיצאו בשנת 2020<br />
       ללא ספק המשחק לא היה מוכן, מלא בבאגים וממש לא החלום שהובטח לכל גיימר<br />
       אולי לא היה צריך ללחוץ עליהם לשחרר את המשחק למרות כל הדחיות והארכות הזמן...<br />
    </h4>
        <h3 style="color:#e86400; text-align:right">אנצ'ארטד הסרט: תמונות חדשות ומידע על הגיבור ניית'ן דרייק</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
       אנשים רבים לא כל כך שמחו כאשר גילו לקהל הרחב שאת ניית'ן יגלם טום הולנד<br />
       עם זאת, מהתמונות ומידע החדש שנגלה לקהל הרחב ניתן להסיק שהסרט מושקע ובעל תקציב גדול<br />
       ומי יודע, אולי טום הולנד יוכיח לכל ההייטרים שהוא השחקן המושלם<br />
       אם הוא מספיק טוב בשביל ניק פיורי, הוא מספיק טוב בשבילנו<br />
    </h4>
        <h3 style="color:#e86400; text-align:right">האתר של סופר נינטנדו וורלד נפתח לקהל הרחב!!!</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
       אתר האינטרנט של פארק השעשועים החדש ביפן SUPER NINTENDO WORLD נפתח לקהל הרחב לשימוש<br />
       שם ניתן יהיה להיכנס, לפחות בתקופה הקשה הזו, לפארק ולראות מה מצפה לנו<br />
    </h4>
<img src="Styles/AlbakaProductions%20pictures/maxresdefault.jpg" width="360" height="202" style="border:0; float:left; padding:20px"/>
<img src="Styles/AlbakaProductions%20pictures/resize.jpg" width="360" height="202" style="border:0; float:left; padding:20px"/>
<img src="Styles/AlbakaProductions%20pictures/UNIVERSAL-STUDIOS-JAPAN.jpg" width="360" height="202" style="border:0; float:left; padding:20px"/>

</asp:Content>
