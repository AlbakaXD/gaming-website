﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Albaka_Productions
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        public string myTime;
        public string loginMsg;
        public string menuBar;
        protected void Page_Load(object sender, EventArgs e)
        {
            myTime = DateTime.Now.ToString();

            loginMsg = "<h3>שלום ";
            loginMsg = loginMsg + Session["fName"].ToString();
            loginMsg = loginMsg + "<h3>";

            if (Session["admin"] == "no")
            {
                menuBar = "[<a href='HomePage.aspx' style='color:#e86400'> דף הבית </a>]";
                menuBar = menuBar + "[<a href='PCreviews.aspx' style='color:#e86400'> PC ביקורות </a>]";
                menuBar = menuBar + "[<a href='PSreviews.aspx' style='color:#e86400'> PlayStation ביקורות </a>]";
                menuBar = menuBar + "[<a href='NintendoReviews.aspx' style='color:#e86400'> Nintendo ביקורות </a>]";
            }
            else
            {
                menuBar = "[<a href='HomePage.aspx' style='color:#e86400'> דף הבית </a>]";
                menuBar = menuBar + "[<a href='PCreviews.aspx' style='color:#e86400'> PC ביקורות </a>]";
                menuBar = menuBar + "[<a href='PSreviews.aspx' style='color:#e86400'> PlayStation ביקורות </a>]";
                menuBar = menuBar + "[<a href='NintendoReviews.aspx' style='color:#e86400'> Nintendo ביקורות </a>]";
                menuBar = menuBar + "[<a href='SimpleQuery.aspx' style='color:#e86400'> טבלת משתמשים </a>]";
                menuBar = menuBar + "[<a href='deleteUser.aspx' style='color:#e86400'> מחיקת משתמש </a>]";
            }
        }
    }
}