﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Albaka_Productions.SignUp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        h1{text-align: center; color:#e86400}
        h2{text-align: center; color:white}
        td{width:150px; color:white;}
        th{color:white; font-family: 'Guttman Yad-Brush';}
        table{margin: 0px auto;}
        .formTbl{margin: 0px auto; border: 1px solid;}
        .formTd{border: 1px solid;}
    </style>

    <body style="background-color: #464646">
        <h1>register form</h1>
        <form method="post" runat="server" onsubmit="return chkForm();">
            <table>
                <tr>
                    <td>שם משתמש:</td>
                    <td><input type="text" id="uName" name="uName"/></td>
                    <td><input type="text" id ="muName" size="40" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled" /></td>
                </tr>
                <tr>
                    <td>שם פרטי:</td>
                    <td><input type="text" id="fName" name="fName"/></td>
                    <td><input type="text" id ="mfName" size="40" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled" /></td>
                </tr>
                <tr>

                    <td>שם משפחה:</td>
                    <td><input type="text" id="sName" name="sName"/></td>
                    <td><input type="text" id ="msName" size="40" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled" /></td>
                </tr>
                <tr>
                    <td>סיסמא:</td>
                    <td><input type="text" id="password" name="password"/></td>
                    <td><input type="text" id ="mPassword" size="40" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled"/></td>
                </tr>
                <tr>
                    <td>וידוא סיסמא:</td>
                    <td><input type="text" id="passwordConfirm" name="passwordConfirm"/></td>
                    <td><input type="text" id ="mPasswordConfirm" size="40" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled" /></td>
                </tr>
                <tr>
                    <td>אימייל:</td>
                    <td><input type="text" id="email" name="email"/></td>
                    <td><input type="text" id ="memail" size="40" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled" /></td>
                </tr>
                <tr>
                    <td>ז'אנר משחקים אהוב</td>
                    <td></td>
                    <td> <input type="radio"  id="FPS" name="type" value="FPS"  /style="color:white;"> FPS </td>
                    <td> <input type="radio"  id="puzzle" name="type" value="puzzle"  /style="color:white;"> puzzle </td>
                    <td> <input type="radio"  id="platform" name="type" value="platform"  /style="color:white;"> platform </td>
                    <td> <input type="radio"  id="horror" name="type" value="horror"  /style="color:white;"> horror </td>
                    <td> <input type="radio"  id="RPG" name="type" value="RPG"  /style="color:white;"> RPG </td>
                    <td> <input type="radio"  id="adventure" name="type" value="adventure"  /style="color:white;"> adventure </td>
                    <td> <input type="radio"  id="co-op" name="type" value="co-op"  /style="color:white;"> co-op </td>
                    <td> <input type="radio"  id="multyplayer" name="type" value="multyplayer"  /style="color:white;"> multyplayer </td>
                    <td> <input type="radio"  id="sports" name="type" value="sports"  /style="color:white;"> sports </td>
                    <td> <input type="radio"  id="VRgame" name="type" value="VR"  /style="color:white;"> VR </td>
                    <td> <input type="radio"  id="openWorld" name="type" value="openWorld"  /style="color:white;"> open world </td>
                    <td> <input type="text"   id ="mtype" name="mtype" size="10" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled" /></td>
                </tr>

                <tr>
                <td> כמה זמן אתה משחק ביום </td>
                <td>
                    <select name="time" id="time">
                        <option value="choose"> בחר תשובה </option>
                        <option value="little"> קצת </option>
                        <option value="littleLot"> קצת הרבה </option>
                        <option value="lotLIttle"> הרבה קצת </option>
                        <option value="Lot"> הרבה </option>
                        <option value="lotLot"> הרבה מאוד </option>
                        <option value="veryLots"> ממש הרבה </option>
                    </select>
                </td>
                <td> <input type="text"   id ="mtime" name="mtime" size="10" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled" /></td>
                <td></td>

                <tr>
                        <tr>
                            <td> בחר קונסולות </td>
                            <td></td>
                            <td><input type="checkbox" name="console" value="1"  /> Playstation</td>
                            <td><input type="checkbox" name="console" value="2"  /> Nintendo</td>
                            <td><input type="checkbox" name="console" value="3"  /> Xbox</td>
                            <td><input type="checkbox" name="console" value="4"  /> PC</td>
                            <td><input type="checkbox" name="console" value="5"  /> VR</td>
                            <td> <input type="text" id ="mconsole" name="mconsole" size="11" style="display:none; background-color:silver;font-weight:bold;" disabled="disabled" /></td>

                        </tr>

                <tr>
                    <td colspan="2" style="text-align: center;"> <input type="submit" name="submit" value="submit" /> </td>
                </tr>
            </table>
            <script>
                function isHebres(mail)
                {
                    var len = mail.length;
                    var i = 0, ch;
                    while (i < len)
                    {
                        ch = mail.charAt(i);
                        if (ch >= 'א' && ch <= 'ת')
                            return false;
                        i++
                    }
                    return true;
                }

                function isValidString(mail)
                {
                    var badChr = "$%^&*()-![]{}<>?";
                    var len = mail.length;
                    var i = 0, pos, ch;
                    while (i < len)
                    {
                        ch = mail.charAt(i);
                        pos = badChr.indexOf(ch);
                        if (pos != -1)
                            return false;
                        i++;
                    }
                    return true;
                }

                function chkForm() {
                    var flag = true;
                    var uName = document.getElementById("uName").value;
                    if (uName.length < 2) {
                        document.getElementById("muName").value = "שם משתמש קצר מידי";
                        document.getElementById("muName").style.display = "inline";
                        flag = false;
                    }
                    else if (uName.length > 10) {
                        document.getElementById("muName").value = "שם משתמש ארוך מידי";
                        document.getElementById("muName").style.display = "inline";
                        flag = false;
                    }
                    else
                        document.getElementById("muName").style.display = "none";


                    var fName = document.getElementById("fName").value;
                    if (fName.length < 2) {
                        document.getElementById("mfName").value = "שם פרטי קצר מידי או לא קיים";
                        document.getElementById("mfName").style.display = "inline";
                        flag = false;
                    }
                    else if (fName.length > 10) {
                        document.getElementById("mfName").value = "שם פרטי ארוך מידי";
                        document.getElementById("mfName").style.display = "inline";
                        flag = false;
                    }
                    else
                        document.getElementById("mfName").style.display = "none";


                    var sName = document.getElementById("sName").value;
                    if (sName.length < 2) {
                        document.getElementById("msName").value = "שם משפחה קצר מידי או לא קיים";
                        document.getElementById("msName").style.display = "inline";
                        flag = false;
                    }
                    else if (sName.length > 10) {
                        document.getElementById("msName").value = "שם משפחה קצר מידי או לא קיים";
                        document.getElementById("msName").style.display = "inline";
                        flag = false;
                    }
                    else
                        document.getElementById("msName").style.display = "none";


                    var email = document.getElementById("email").value;
                    var size = email.length;
                    var atSign = email.indexOf('@');
                    var dotSign = email.indexOf('.', atSign);

                    var msg = " ";
                    if (size < 6)
                        msg = "כתובת האימייל קצרה מידי או לא קיימת";
                    else if (atSign == -1)
                        msg = "הסימן @ לא מופיע בכתובת האימייל";
                    else if (atSign != email.lastIndexOf('@'))
                        msg = "שני סימני @ או יותר לא תקפים בכתובת אימייל";
                    else if (atSign < 2 || email.lastIndexOf('@') == size - 1)
                        msg = "הסימן @ לא יכול להופיע בתחילת האימייל או בסופו";
                    else if (email.indexOf('.') == 0 || email.lastIndexOf('.') == size - 1)
                        msg = "נקודה לא יכולה להיות בתחילת אימייל או בסופו";
                    else if (dotSign - atSign <= -1)
                        msg = "הנקודה חייבת להיות לפחות שני תווים לאחר הסימן @";
                    else if (isHebres(email) == false)
                        msg = "כתובת האימייל לא יכולה להכיל אותיות בעברית"
                    else if (isValidString(email) == false)
                        msg = "כתובת האימייל לא יכולה להכיל תווים שהם לא נקודה או שטרודל"

                    if (msg != " ") {
                        document.getElementById("memail").value = msg;
                        document.getElementById("memail").style.display = "inline";
                        flag = false;
                    }
                    else
                        document.getElementById("memail").style.display = "none";


                    var password = document.getElementById("password").value;
                    var passCheckletter = false;
                    var passChecknumber = false;
                    password_length = password.length;
                    for (var i = 0; i < password_length; i++) {
                        if (password[i] <= 'z' && password[i] >= 'a' || password[i] <= 'Z' && password[i] >= 'A') {
                            passCheckletter = true;
                        }
                        if (password[i] <= 9 && password[i] >= 0) {
                            passChecknumber = true;
                        }
                    }
                    if (password_length <= 6 || password_length >= 12) {
                        document.getElementById("mPassword").value = "אורך סיסמא לא תקין";
                        document.getElementById("mPassword").style.display = "inline";
                        flag = false;
                    }
                    else if (passCheckletter == false) {
                        document.getElementById("mPassword").value = "בסיסמא חייבת להיות אות אחת לפחות";
                        document.getElementById("mPassword").style.display = "inline";
                        flag = false;
                    }
                    else if (passChecknumber == false) {
                        document.getElementById("mPassword").value = "בסיסמא חייבת להיות ספרה אחת לפחות";
                        document.getElementById("mPassword").style.display = "inline";
                        flag = false;
                    }
                    else
                        document.getElementById("mPassword").style.display = "none";


                    var passwordConfirm = document.getElementById("passwordConfirm").value;
                    var count = 0;
                    for (var i = 0; i < password_length; i++)
                    {
                        if (password[i] == passwordConfirm[i])
                        {
                            count = count + 1;
                        }
                    }
                    if (count != password_length)
                    {
                        document.getElementById("mPasswordConfirm").value = "הסיסמאות לא דומות";
                        document.getElementById("mPasswordConfirm").style.display = "inline";
                        flag = false;
                    }
                    else
                        document.getElementById("mPasswordConfirm").style.display = "none";


                    var type = document.getElementsByName("type");
                    var ansChecked = false;
                    for (var i = 0; i < type.length; i++)
                    {
                        if (type[i].checked == true)
                        {
                            ansChecked = true;
                        }
                    }
                    if (ansChecked == false)
                    {
                        document.getElementById("mtype").value = "ז'אנר לא נבחר";
                        document.getElementById("mtype").style.display = "inline";
                        flag = false;
                    }
                    else
                        document.getElementById("mtype").style.display = "none";


                    var time = document.getElementById("time");
                    if (time.selectedIndex == 0)
                    {
                        document.getElementById("mtime").value = "תשובה לא נבחרה";
                        document.getElementById("mtime").style.display = "inline";
                        flag = false;
                    }
                    else
                        document.getElementById("mtime").style.display = "none";


                    var console = document.getElementsByName("console");
                    var consoleChecked = false;
                    for (var i = 0; i < console.length; i++) {
                        if (console[i].checked)
                        {
                            consoleChecked = true;
                        }
                    }
                    if (consoleChecked == false) {
                        document.getElementById("mconsole").value = "לא נבחרה קונסולה ";
                        document.getElementById("mconsole").style.display = "inline";
                        flag = false;
                    }
                    else
                    {
                        document.getElementById("mconsole").style.display = "none";
                    }
                    return flag;
                }
            </script>
            <% =st %>
        </form>
</asp:Content>