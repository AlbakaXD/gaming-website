﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="PSreviews.aspx.cs" Inherits="Albaka_Productions.PSreviews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <h2 style="color:#e86400; direction:rtl; text-align:center">ביקורות PlayStation</h2>
    
    <img src="Styles/AlbakaProductions%20pictures/the-last-guardian-desktop-1920x1080-wallpaper-01-04nov16.jpg" width="360" height="202" style="border:0; float:left; padding:20px"/>
    <h3 style="color:#e86400; text-align:right">The Last Guardian</h3>
    <h4 style="color:white; direction:rtl; text-align:right">
        משחק כל כך יפה שלא יכולתי שלא להישבות בקסמיו!!!<br />
        The Last Guardian ל-PlayStation 4 הוא רימייק למשחק שיצא ל-PlayStation 2, מה חדש ומה השתנה?
    </h4>
    <p style="color:white; direction:rtl; text-align:right">
        The Last Guardian הוא משחק Puzzle-Adventure בלעדי ל-PlayStation 4<br />
        המשחק הוא פורט שהגיע מה-PlayStation 2, יצאתי לבדוק מה השתנהת ומה נשמר<br />
        המשחק הזה לא דומה לרבים בתעשיית המשחקים<br />
        הוא רגוע ושליו ולעיתים מפחיד ומלחיץ...<br />
        לביקורת המלאה:<br />
        <a href="TheLastGuardian.aspx" style="color:#e86400">The Last Guardian</a><br />
    </p>


</asp:Content>
